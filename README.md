# lds-autodelete

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg?style=flat-square)](https://gitlab.com/loodos/library/lds-autodelete/blob/master/LICENSE)

lds-autodelete is a PowerShell script that automatically delete old log files to save space.

You can run this script on the terminal or create an automated scheduled task.

## Configuration

| Parameter  | Mandatory | Default | Description                                      |
| ---------- | --------- | ------- | ------------------------------------------------ |
| beforeDays | false     | 30      | Set to delete files older than x days only       |
| fileFormat | false     | *.txt   | Set to delete files that only match this format. |

## Usage

`.\lds-autodelete.ps1 -beforeDays 30 -fileFormat "lds-*.log"`

> In the above sample; files that are older than `30 days` and the file name starts with `lds-` and extension is `log` will be deleted.

`.\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddMonths(-3).toString("yyyy-MM"))-*.zip"`

> In the above sample; files whose names start with `3 months` old from today will be deleted.

`13..36 | % { .\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddMonths(-$PSItem).toString("yyyy-MM"))-*.zip" }`

> In the above sample; files whose names start with from `13 months` to `36 months` old from today will be deleted.

## Creating a scheduling task

You can automate delete process with a scheduling task that runs every month.

```
The task must be `run as Administrator user or group` and `run with highest privileges` for deletion.
```

### Create new Action

| | |
|-|-|
| Action | Start a Program |
| Program/script | PowerShell |
| Add arguments | `.\lds-autodelete.ps1 -beforeDays 30 -fileFormat "lds-*.log"` |
| Start in | `C:\Loodos\ps-scripts\` |
| | |

### Create new Trigger

| | |
|-|-|
| Begin the task | On a schedule |
| Monthly | Set Days = `1` and select all months  |
| | |

## Who is using this

Here are companies, teams, projects or libraries that use this project in their work.

- [Loodos](https://loodos.com)
- [Vodafone TR, Yanımda Mobile App](https://vodafone.com.tr)
- [Beşiktaş, Vodafone Park Mobile App](http://www.vodafonearena.com.tr/)
- and more...

Please add your own! Send a Pull Request or [contact us](https://loodos.com/#letsconnect).

Thanks!

## Commercial Support

This is an open source project, so the amount of time we have available to help resolve your issue is often limited as all help is provided on a volunteer basis. If you want to get priority help, need to get up to speed quickly, require some training or mentoring, or need full 24 x 7 production support you could [contact us](https://loodos.com/#letsconnect) with Commercial Offerings.

## Contributing

This is an open source project and we love to receive contributions from our community — you!

There are many ways to contribute, from writing tutorials or blog posts, improving the documentation, submitting bug reports and feature requests or writing code.

## License

This is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://gitlab.com/loodos/library/lds-autodelete/blob/master/LICENSE)
