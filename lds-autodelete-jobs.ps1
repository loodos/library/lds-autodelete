
.\lds-autodelete.ps1 -beforeDays 90 -fileFormat "*-lds.zip"
.\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddDays(-90).toString("yyyy-MM"))-*lds.zip"
.\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddMonths(-3).toString("yyyy-MM"))-*.zip"

13..36 | % { .\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddMonths(-$PSItem).toString("yyyy-MM"))-*.zip" }
