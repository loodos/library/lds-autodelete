#usage .\lds-autodelete.ps1 -beforeDays 90 -fileFormat "*-lds.zip"
#usage .\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddDays(-90).toString("yyyy-MM"))-*lds.zip"
#usage .\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddMonths(-3).toString("yyyy-MM"))-*.zip"
#usage 3..10 | % { .\lds-autodelete.ps1 -beforeDays 0 -fileFormat "$((Get-Date).AddMonths(-$PSItem).toString("yyyy-MM"))-*.zip" }
Param(
	[Parameter(Mandatory=$False)]
	[Int32]$beforeDays=30,
	[Parameter(Mandatory=$False)]
	[String]$fileFormat="*.txt"
) 

$limit = (Get-Date).AddDays(-$beforeDays)

$files = Get-ChildItem -Path ".\" -Recurse -File -Filter $fileFormat | 
Where-Object { !$_.PSIsContainer -and $_.LastwriteTime -lt $limit } 

ForEach ($file in $files) {
  Write-Host "delete:" $file.FullName
  Remove-Item $file.FullName
}
