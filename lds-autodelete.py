#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import collections
from datetime import datetime, date, timedelta
import re
import fnmatch
import subprocess
import logging
import cronex

def delete_files(files):
    for file_name in files:
        logging.debug("file deleted: {0}".format(file_name))
        os.remove(file_name)
        pass

def filter_by_date(src_folder, filter, archive_date):
    if args.deleteBeforeDays > 0:
        deleteBeforeDate = extractDaysFromToday(args.deleteBeforeDays)
        logging.info("Delete Before: {0}".format(deleteBeforeDate))
    else:
        deleteBeforeDate = datetime.combine(datetime.min.date(), datetime.min.time())

    files = []
    logging.info("searching: {0}".format(src_folder))
    job = cronex.CronExpression(args.cron)
    for file_name in os.listdir(src_folder):
        full_name = os.path.join(src_folder, file_name)
        if os.path.isfile(full_name):
            if fnmatch.fnmatch(file_name, filter):
                keeped = True
                file_date = datetime.fromtimestamp(os.path.getmtime(full_name))
                if file_date < archive_date:
                    if file_date < deleteBeforeDate or job.check_trigger((file_date.year, file_date.month, file_date.day, 0, 0)) == False:
                        logging.debug("file found: {0} - {1} ".format(file_date, full_name))
                        files.append(full_name)
                        keeped = False
                if keeped:
                    logging.debug("-- file keeped: {0} - {1} ".format(file_date, full_name))
                
    logging.info("{0} files found".format(len(files)))
    return files

def main():
    logging.basicConfig(format='[%(levelname)s] %(message)s', level=logging.DEBUG)
    logging.info("STARTING")
    keepToDay = extractDaysFromToday(args.keepDays)
    logging.info("Keep To: {0}".format(keepToDay))
    files = filter_by_date(args.path, args.filter, keepToDay)
    delete_files(files)

def extractDaysFromToday(_days):
    """
        days: int
        returns datetime
    """
    _date = date.today() - timedelta(days=_days)
    _date = datetime.combine(_date, datetime.min.time())
    return _date

def is_valid_dir(parser, arg):
    """
    Check if arg is a valid directory that already exists on the file system.

    Parameters
    ----------
    parser : argparse object
    arg : str

    Returns
    -------
    arg
    """
    arg = os.path.abspath(arg)
    if not os.path.exists(arg):
        parser.error("The directory [%s] does not exist!" % arg)
    elif not os.path.isdir(arg):
        parser.error("[%s] not a directory!" % arg)
    else:
        return arg


def get_parser():    
    parser = argparse.ArgumentParser(prog='python log-auto.py', usage='%(prog)s [options]')
    parser.add_argument("-p", "--path",
                        dest="path",
                        default="./",
                        type=lambda x: is_valid_dir(parser, x),
                        help="Path that contain files. Default Value: './'",
                        metavar="DIR")
    parser.add_argument("-kd", "--keepDays",
                        dest="keepDays",
                        default=30,
                        type=int,
                        help="Default Value: 30")
    parser.add_argument("-dbd", "--deleteBeforeDays",
                        dest="deleteBeforeDays",
                        default=0,
                        type=int,
                        help="If 0 nothing will be deleted. Default Value: 0")
    parser.add_argument("-f", "--filter",
                        dest="filter",
                        default="*.log",
                        help="Filter. Default Value: '*.log'")

    parser.add_argument("-c", "--cron",
                        dest="cron",
                        default='* * * * 0',
                        help="Cron argument that using for file time check for deletion. Default Value: '* * * * 0' (every sunday)")

    return parser



if __name__ == "__main__":
    args = get_parser().parse_args()
    main()